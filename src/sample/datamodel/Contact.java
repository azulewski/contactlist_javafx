package sample.datamodel;

import javafx.beans.property.SimpleStringProperty;
import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class Contact {
    private SimpleStringProperty firstName = new SimpleStringProperty("");
    private SimpleStringProperty lastName = new SimpleStringProperty("");
    private SimpleStringProperty phoneName = new SimpleStringProperty("");
    private SimpleStringProperty notes = new SimpleStringProperty("");



}
