module Notepad {

    requires javafx.fxml;
    requires javafx.controls;
    requires lombok;

    opens sample;
}